/*
 * Copyright 2019 Simon Redman <simon@ergotech.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.kde.kdeconnect.Plugins.SMSPlugin;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.content.ContextCompat;

import com.klinker.android.send_message.Utils;

import org.kde.kdeconnect.Helpers.NotificationHelper;
import org.kde.kdeconnect.Helpers.SMSHelper;
import org.kde.kdeconnect_tp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Receiver for notifying user when a new MMS has been received by the device. By default it will
 * persist the message to the internal database. We also need to add functionality for giving a
 * notification to the user that it has been received.
 */
public class MmsReceivedReceiver extends com.klinker.android.send_message.MmsReceivedReceiver {

    @Override
    public void onMessageReceived(Context context, Uri messageUri) {
        Log.v("MmsReceived", "message received: " + messageUri.toString());

        Cursor lastMessage = SmsMmsUtils.getMmsMessage(context, messageUri);

        if (lastMessage != null && lastMessage.moveToFirst()) {
            ContentValues values = SmsMmsUtils.processMessage(context, lastMessage).get(0);

            createMmsNotification(context, values);
        } else {
            try {
                lastMessage.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onError(Context context, String error) {
        Log.v("MmsReceived", "error: " + error);

        Cursor lastMessage = SmsMmsUtils.getLastMmsMessage(context);
        if (lastMessage != null && lastMessage.moveToFirst()) {
            Uri uri = Uri.parse("content://mms/" + lastMessage.getLong(0));

            if (lastMessage != null && lastMessage.moveToFirst()) {
                ContentValues values = SmsMmsUtils.processMessage(context, lastMessage).get(0);
                createMmsNotification(context, values);
            }
        } else {
            try {
                lastMessage.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * some carriers will download duplicate MMS messages without this ACK. When using the
     * system sending method, apparently Google does not do this for us. Not sure why.
     * We might have to have users manually enter their APN settings if we cannot get them
     * from the system somehow.
     */
    @Override
    public MmscInformation getMmscInfoForReceptionAck() {
        if (SMSHelper.mmscUrl != null || SMSHelper.mmsProxy != null || SMSHelper.mmsPort != null) {
            try {
                return new MmscInformation(SMSHelper.mmscUrl, SMSHelper.mmsProxy, Integer.parseInt(SMSHelper.mmsPort));
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private void createMmsNotification(Context context, ContentValues values) {
        String addresses = values.getAsString(Telephony.Mms.Addr.ADDRESS);
        ArrayList<String> addressList = new ArrayList<>(Arrays.asList(addresses.split(",")));
        StringBuilder addressBuilder = new StringBuilder();

        String senderName = SmsMmsUtils.getContactName(context, addressList.get(0));

        for (String address : addressList) {
            addressBuilder.append(SmsMmsUtils.getContactName(context, address));
            addressBuilder.append(", ");
        }

        String contactNameLIst = addressBuilder.toString();
        if (contactNameLIst.contains(", ")) {
            contactNameLIst = contactNameLIst.substring(0, contactNameLIst.length() - 2);
        }

        int notificationId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notificationId = (int) Utils.getOrCreateThreadId(context, (Set<String>) addressList);
        } else {
            notificationId = (int) System.currentTimeMillis();
        }

        Person sender = new Person.Builder()
                .setName(senderName == null ? addressList.get(0) : senderName)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.MessagingStyle.Message message = new NotificationCompat.MessagingStyle.Message(
                values.getAsString(Telephony.Mms.Part.TEXT),
                System.currentTimeMillis(),
                sender);

        String mimeType = values.getAsString(Telephony.Mms.Part.CONTENT_TYPE);
        if (!mimeType.equals(MimeType.TEXT_PLAIN)) {
            message.setData(values.getAsString(Telephony.Mms.Part.CONTENT_TYPE), Uri.parse(values.getAsString(Telephony.Mms.Part._DATA)));
        }

        Intent resultIntent = new Intent(context, NotificationReplyReceiver.class);
        resultIntent.setAction(NotificationReplyReceiver.SMS_MMS_REPLY_ACTION);
        resultIntent.putExtra(NotificationReplyReceiver.NOTIFICATION_ID, notificationId);
        resultIntent.putExtra(NotificationReplyReceiver.ADDRESS_LIST, addressList);

        PendingIntent resultPendingIntent = PendingIntent.getBroadcast(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteInput remoteInput = new RemoteInput.Builder(NotificationReplyReceiver.KEY_TEXT_REPLY)
                .setLabel(context.getString(R.string.sms_reply_label))
                .build();

        NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(0, NotificationReplyReceiver.REPLY, resultPendingIntent)
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(true)
                .build();

        NotificationCompat.MessagingStyle messageStyle = NotificationReplyReceiver.restoreActiveMessagingStyle(notificationId, notificationManager);

        // When no active notification is found for matching conversation create a new one
        if (messageStyle == null) {
            messageStyle = new NotificationCompat.MessagingStyle(sender)
                    .setConversationTitle(contactNameLIst);

            if (addressList.size() > 1) {
                messageStyle.setGroupConversation(true);
            }
        }
        messageStyle.addMessage(message);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationReplyReceiver.CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_sms_notification)
                .setColor(ContextCompat.getColor(context, R.color.primary))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(messageStyle)
                .addAction(replyAction);

        NotificationHelper.notifyCompat(notificationManager, notificationId, builder.build());
    }
}